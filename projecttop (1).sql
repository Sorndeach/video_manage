-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2017 at 06:30 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projecttop`
--

-- --------------------------------------------------------

--
-- Table structure for table `office`
--

CREATE TABLE `office` (
  `office_id` varchar(20) NOT NULL,
  `office_name` varchar(255) NOT NULL,
  `office_address` varchar(255) NOT NULL,
  `office_phone` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `office`
--

INSERT INTO `office` (`office_id`, `office_name`, `office_address`, `office_phone`) VALUES
('FC0001', 'ทดสอบชื่หน่วยงาน', 'addresssssddddd', '0888888888'),
('FC0002', 'ทดสอบชื่หน่วยงาน', 'ทดสอบ2', '0999999999'),
('FC0003', 'ทดสอบชื่หน่วยงาน3', 'ทดสอบที่อยู่3', '8888888888');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` varchar(20) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_user` varchar(30) NOT NULL,
  `user_pass` varchar(30) NOT NULL,
  `user_phone` varchar(10) NOT NULL,
  `user_add` varchar(500) NOT NULL,
  `user_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_user`, `user_pass`, `user_phone`, `user_add`, `user_status`) VALUES
('US0001', 'Administrator11', 'admin', '123456', '0899999999', 'Test address01', 'admin'),
('US0002', 'Test namedddddddd', 'sadsad', '123456', '0888888888', 'Address ', 'user'),
('US0003', 'Test name', 'test2', '123456', '0000000000', 'teataet', 'user'),
('US0004', 'ไพฑูรย์ แก้วทอง', 'test4', '123456', '9999999999', 'address', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `video_id` varchar(20) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `video_url` varchar(255) NOT NULL,
  `video_description` varchar(255) NOT NULL,
  `office_id` varchar(10) NOT NULL,
  `vtype_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`video_id`, `video_name`, `video_url`, `video_description`, `office_id`, `vtype_id`) VALUES
('VD0001', 'ทดสอบชื่อวีดีโอ1', 'https://www.youtube.com/watch?v=ZZpFsGe-YOY', 'กกกกกกกกกกกกกกกกกกกกกก', 'FC0001', 'VT0001'),
('VD0002', 'ทดสอบชื่อวีดีโอ2', 'https://www.youtube.com/watch?v=ZZpFsGe-YOY', 'ffffffffffffffffffff', 'FC0002', 'VT0002');

-- --------------------------------------------------------

--
-- Table structure for table `video_type`
--

CREATE TABLE `video_type` (
  `vtype_id` varchar(20) NOT NULL,
  `vtype_name` varchar(255) NOT NULL,
  `vtype_detail` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `video_type`
--

INSERT INTO `video_type` (`vtype_id`, `vtype_name`, `vtype_detail`) VALUES
('VT0001', 'ประเภท1ss', 'รายละเอียดประเภท1'),
('VT0002', 'ประเภท2', 'รายละเอียดประเภท2'),
('VT0003', 'ประเภท33333', 'รายละเอียดประเภท33333'),
('VT0004', 'ประเภท4', 'รายละเอียดประเภท4'),
('VT0005', 'ประเภท5', 'รายละเอียดประเภท5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`office_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `video_type`
--
ALTER TABLE `video_type`
  ADD PRIMARY KEY (`vtype_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
