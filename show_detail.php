<?php
    session_start();
    date_default_timezone_set("Asia/Bangkok");
    include('dbconn.inc.php');
    if( !isset($_SESSION['user_id']) && empty($_SESSION['user_id']) ){
        echo "<script>
        window.location.href='login.php';
        </script>";
        exit();
    }

    if($_GET){
        $video_id = $_GET['video_id'];
        $user_id = $_SESSION['user_id'];
        $date = date('d-m-Y');
        $time = date('H:i:s');

        $sql = "INSERT INTO history_video VALUES('','$video_id','$user_id','$date','$time')";
        @$mysqli->query($sql) or die($mysqli->error);

        $data = query1("SELECT * FROM video WHERE video_id = '$video_id'");
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <? include('_navtop.php'); ?>
        <div class="row" style="padding: 50px 0px 50px 0px;">
            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;text-align: center;">
                <h2 style="color:#eea236;"><?php echo $data->video_name ;?></h2>
            </div>
            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;text-align: center;">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/<?php echo $data->video_url ;?>"></iframe>
            </div>
            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;text-align: center;">
                <h2 style="color:#eea236;">รายละเอียด</h2>
                <p>
                    <?php echo $data->video_description ;?>
                </p>
            </div>
        </div>
    </body>
</html>
