<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php" style="padding: 0px;">
        	<img src="../images/logo3.png" style="height: 50px;margin-left: 30px;" alt="" />
        </a>
    </div>

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
            </ul>
        </li>
    </ul>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                </li>
                <li>
                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> หน้าแรก</a>
                </li>
                <li>
                    <a href="user_manage.php"><i class="fa fa-user"></i> จัดการสมาชิก</a>
                </li>
                <li>
                    <a href="video_list.php"><i class="fa fa-video-camera"></i> วิดีโอแพทยแ์ผนไทย</a>
                </li>
                <li>
                    <a href="type_video_list.php"><i class="fa fa-dashboard fa-fw"></i> เพิ่มประเภทวีดีโอ</a>
                </li>
                <li>
                    <a href="office_list.php"><i class="fa fa fa-building-o"></i> เพิ่มหน่วยงาน</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-navicon"></i> รายงาน<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="report_user.php">รายงานสมาชิก</a>
                        </li>
                        <li>
                            <a href="report_typevideo.php">รายงานประเภทวีดีโอ</a>
                        </li>
                        <li>
                            <a href="report_office.php">รายงานหน่วยงาน</a>
                        </li>
                        <li>
                            <a href="report_video.php">รายงานวีดีโอ</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>