<?php
    session_start();
    include('../dbconn.inc.php');

    if( isset($_POST['video_name']) && !empty($_POST['video_name']) ){
        $video_id = $_POST['video_id'];
        $video_name = $_POST['video_name'];
        $video_url = $_POST['video_url'];
        $video_description = $_POST['video_description'];
        $vtype_id = $_POST['vtype_id'];
        $office_id = $_POST['office_id'];
        $for = $_POST['for'];

        if( !empty($for) && $for == 'edit' ){
            $sql = "UPDATE video SET 
            video_name = '$video_name',
            video_url = '$video_url',
            video_description = '$video_description',
            vtype_id = '$vtype_id',
            office_id = '$office_id'
            WHERE video_id = '$video_id'";
            @$mysqli->query($sql) or die($mysqli->error);
        }else{
            $sql = "INSERT INTO video VALUES('$video_id','$video_name','$video_url','$video_description','$office_id','$vtype_id')";
            @$mysqli->query($sql) or die($mysqli->error);
        }
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="video_list.php";
        </script>
<?php
    }

    if($_GET){
        $video_id = $_GET['video_id'];
        $for = $_GET['for'];

        if( $for == 'edit' ){
            $data = query1("SELECT * FROM video WHERE video_id = '$video_id'");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</title>
    <?php include('_css.php'); ?>
    <style type="text/css">
        tr th{
            text-align: center;
        }
    </style>
</head>

<body>

    <div id="wrapper">
        
    <?php include('_navtop.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> เพิ่มวีดีโอ
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>รหัส</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                        <input type="text" name="video_id" value="<?php echo isset($data->video_id)? $data->video_id : ''; ?>" class="form-control" id="video_id" placeholder="ระบุรหัส" maxlength="20" <?php echo isset($data->video_id)? 'readonly' : 'required'; ?> >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ชื่อวีดีโอ</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="video_name" value="<?php echo isset($data->video_name)? $data->video_name : ''; ?>" class="form-control" id="video_name" placeholder="ระบุชื่อประเภท" maxlength="200" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Url Youtube</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <input type="text" name="video_url" value="<?php echo isset($data->video_url)? $data->video_url : ''; ?>" class="form-control" id="video_url" placeholder="ระบุ Url youtube" maxlength="200" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ประเภทวีดีโอ</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                        <select class="form-control" name="vtype_id">
                                            <option>--- เลือกประเภทวีดีโอ ---</option>
                                            <?php
                                                $query = query2("SELECT * FROM video_type");
                                                while($data_select = $query->fetch_object()){
                                            ?>
                                                <option value="<?=$data_select->vtype_id?>" <?php echo isset($data->vtype_id) && $data_select->vtype_id == $data->vtype_id ? 'selected' : ''; ?> ><?=$data_select->vtype_name?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>หน่วยงาน</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-flag-checkered"></i></span>
                                        <select class="form-control" name="office_id">
                                            <option>--- เลือกหน่วยงาน ---</option>
                                            <?php
                                                $query = query2("SELECT * FROM office");
                                                while($data_select = $query->fetch_object()){
                                            ?>
                                                <option value="<?=$data_select->office_id?>" <?php echo isset($data->office_id) && $data_select->office_id == $data->office_id ? 'selected' : ''; ?> ><?=$data_select->office_name?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>รายละเอียด</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-navicon"></i></span>
                                        <textarea class="form-control" name="video_description" placeholder="ระบุรายละเอียด" maxlength="500"><?php echo isset($data->video_description)? $data->video_description : ''; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="hidden" name="for" value="<?php echo isset( $for )? $for : ''; ?>">
                                <button type="submit" class="btn btn-primary">ตกลง</button>
                                <a href="add_video.php" class="btn btn-warning">ยกเลิก</a>
                            </div>
                        </form>
                        </div>
                    </div>
                </div><!-- col-lg-12 -->
                
            </div><!-- row -->
        </div><!-- page-wrapper -->
    </div>
    <?php include('_js.php') ?>
</body>

</html>
