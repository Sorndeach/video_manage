<?php
    session_start();
    include('../dbconn.inc.php');

    if($_GET){
        $user_id = $_GET['user_id'];
        $data = query1("SELECT * FROM user WHERE user_id = '$user_id'");
    }
    if($_POST){
        $user_id = $_POST['user_id'];
        $user_name = $_POST['user_name'];
        $user_pass = $_POST['user_pass'];
        $user_phone = $_POST['user_phone'];
        $user_add = $_POST['user_add'];
        $user_status = $_POST['user_status'];

        $sql = "UPDATE user 
        SET user_name = '$user_name',
        user_pass = '$user_pass',
        user_phone = '$user_phone',
        user_add = '$user_add',
        user_status = '$user_status'
        WHERE user_id = '$user_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="user_manage.php";
        </script>
<?php
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</title>
    <?php include('_css.php'); ?>
</head>

<body>

    <div id="wrapper">
        
    <?php include('_navtop.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> แก้ไขสมาชิก
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="form-group">
                                <label>รหัส</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                    <input type="text" name="user_id" class="form-control" id="user_id" value="<?php echo $data->user_id ;?>" placeholder="ระบุชื่อ-นามสกุล" maxlength="200" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อ-นามสกุล</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="user_name" class="form-control" id="user_name" value="<?php echo $data->user_name ;?>" placeholder="ระบุชื่อ-นามสกุล" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อผู้ใช้</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                    <input type="text" name="user_user" class="form-control" id="user_user" value="<?php echo $data->user_user ;?>" readonly placeholder="รุบุชื่อผู้ใช้" maxlength="30" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>รหัสผ่าน</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input type="text" name="user_pass" class="form-control" id="user_pass" value="<?php echo $data->user_pass ;?>" placeholder="ระบุรหัสผ่าน" maxlength="30" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>โทรศัพท์</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                                    <input type="text" name="user_phone" class="form-control" id="user_phone" value="<?php echo $data->user_phone ;?>" placeholder="ระบุโทรศัพท์" maxlength="10" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <textarea class="form-control" name="user_add" placeholder="ระบุที่อยู่" maxlength="500"><?php echo $data->user_add ;?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>สถนานะผู้ใช้</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                    <select class="form-control" name="user_status" required>
                                        <option value="">----เลือกสถนานะผู้ใช้----</option>
                                        <option value="admin" <?php echo $data->user_status == "admin"? "selected" : "" ;?> >Admin</option>
                                    <option value="user" <?php echo $data->user_status == "user"? "selected" : "" ;?> >User</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ตกลง</button>
                        </form>
                        </div>
                    </div>
                </div><!-- col-lg-12 -->
            </div><!-- row -->
        </div><!-- page-wrapper -->
    </div>
    <?php include('_js.php') ?>
</body>

</html>
<script type="text/javascript">
    
</script>
