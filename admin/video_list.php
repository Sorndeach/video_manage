<?php
    session_start();
    include('../dbconn.inc.php');
    
    if($_GET){
        $video_id = $_GET['video_id'];
        $sql = "DELETE FROM video WHERE video_id = '$video_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('ลบข้อมูลเรียบร้อย');
            window.location.href="video_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</title>
    <?php include('_css.php'); ?>
    <style type="text/css">
        tr th{
            text-align: center;
        }
    </style>
</head>

<body>

    <div id="wrapper">
        
    <?php include('_navtop.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> รายการวีดีโอ
                            <div class="pull-right">
                                <a href="add_video.php" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3" style="padding: 0px;margin-bottom: 30px;margin-top: 20px;">
                                <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>"  method="post">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="filter_name" placeholder="กรอกข้อมูลเพื่อค้นหา...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-warning" type="submit">ค้นหา</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12" style="padding: 0px;">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="vtype-table">
                                    <thead>
                                        <tr>
                                            <th>รหัส</th>
                                            <th>ชื่อ</th>
                                            <th>รายละเอียด</th>
                                            <th>แก้ไข/ลบ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if( isset($_POST['filter_name']) && !empty($_POST['filter_name']) ){
                                            $filter_name = $_POST['filter_name'];
                                            $sql = "SELECT * FROM video
                                            WHERE video_id LIKE '%$filter_name%'
                                            OR video_name LIKE '%$filter_name%'
                                            OR video_description LIKE '%$filter_name%'
                                            ";
                                        }else{
                                            $sql = "SELECT * FROM video";
                                        }
                                        $query = $mysqli->query($sql);
                                        while($data = $query->fetch_object()) :
                                    ?>
                                        <tr>
                                            <th scope="row"><?php echo $data->video_id?></th>
                                            <td><?php echo $data->video_name?></td>
                                            <td><?php echo $data->video_description?></td>
                                            <td style="text-align: center;">
                                                <div class="btn-group btn-group-sm" role="group">
                                                    <a class="btn btn-success" href="add_video.php?video_id=<?php echo $data->video_id?>&for=edit">แก้ไข</a>
                                                    <a class="btn btn-danger" href="add_video.php?video_id=<?php echo $data->video_id?>&for=del" onclick="return confirm('คุณต้องการลบข้อมูลของ <?php echo $data->video_name?>')">ลบ</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php
                                        endwhile;
                                    ?>
                                    </tbody>
                                </table> 
                            </div>
                        </div>
                    </div>
                </div><!-- col-lg-12 -->
            </div><!-- row -->
        </div><!-- page-wrapper -->
    </div>
    <?php include('_js.php') ?>
</body>

</html>
