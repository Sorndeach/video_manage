<?php  
    include('../dbconn.inc.php');
?>  
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</title>
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/css-report.css" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">
    </head>

    <body>
        <div class="for-print no-print">
            <button onClick="javascript:window.print()" class="btn btn-default"><i class="fa fa-print"></i> ปริ้น</button>
            <a href="index.php" class="btn btn-default"><i class="fa fa-undo"></i> กลับ</a>
        </div>
        <div class="book">
            <?php
                $per_reccord = 20; //FIX PER PAGE
                $chk_num = count3("SELECT * FROM user ORDER BY user_id ASC");
                $chk_num = ceil( $chk_num / $per_reccord ); 
                $rob = 1; $limit = 0; $num = 1;
                while ( $rob <= $chk_num) :
            ?>
                <div class="page">
                    <div class="subpage">
                        <div class="col-md-12" style="padding: 0px; text-align: center;">
                            <img src="../img/pbru_logo.gif" width="70px">
                            <p>มหาวิทยาลัยราชภัฏเพชรบุรี</p>
                            <p>38 หมู่ 8 ถ.เพชรบุรี-หาดเจ้าสำราญ ต.นาวุ้ง อ.เมือง จ.เพชรบุรี</p>
                            <p>รายงานสมาชิก</p>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>รหัส</th>
                                    <th>ชื่อ-นามสกุล</th>
                                    <th>เบอร์โทร</th>
                                    <th>ที่อยู่</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM user ORDER BY user_id ASC LIMIT $limit, $per_reccord";
                                $query=query2($sql);
                                //$num = 1;
                                while($data = $query->fetch_object()) :
                                ?>
                                <tr>
                                    <td><?php echo $num; ?> </td>
                                    <td><?php echo $data->user_id?></td>
                                    <td><?php echo $data->user_name?></td>
                                    <td><?php echo $data->user_add?></td>
                                    <td><?php echo $data->user_phone?></td>
                                </tr> 
                                <?php
                                    $num = $num+1;
                                    $limit = $limit+1;
                                endwhile;
                                ?>

                                <!-- SUM DATA ALL  -->
                                <?php if( $rob == $chk_num) :?>
                                <tr>
                                    <th colspan="5">รวม <?php echo $num-1; ?> รายการ</th>
                                </tr>
                                <?php endif; ?>

                            </tbody>
                        </table>
                    </div>    
                </div>
            <?
                $rob++;
                endwhile;
            ?>
            
        </div>
    </body>
</html>