<?php
    session_start();
    include('../dbconn.inc.php');

    if( isset($_POST['vtype_name']) && !empty($_POST['vtype_name']) ){
        $vtype_id = $_POST['vtype_id'];
        $vtype_name = $_POST['vtype_name'];
        $vtype_detail = $_POST['vtype_detail'];
        $for = $_POST['for'];

        if( !empty($for) && $for == 'edit' ){
            $sql = "UPDATE video_type SET 
            vtype_name = '$vtype_name', 
            vtype_detail = '$vtype_detail'
            WHERE vtype_id = '$vtype_id'";
            @$mysqli->query($sql) or die($mysqli->error);
        }else{
            $sql = "INSERT INTO video_type VALUES('$vtype_id','$vtype_name','$vtype_detail')";
            @$mysqli->query($sql) or die($mysqli->error);
        }
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="type_video_list.php";
        </script>
<?php
    }

    if($_GET){
        $vtype_id = $_GET['vtype_id'];
        $for = $_GET['for'];

        if( $for == 'edit' ){
            $for = 'edit';
            $data = query1("SELECT * FROM video_type WHERE vtype_id = '$vtype_id'");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</title>
    <?php include('_css.php'); ?>
    <style type="text/css">
        tr th{
            text-align: center;
        }
    </style>
</head>

<body>

    <div id="wrapper">
        
    <?php include('_navtop.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> จัดการประเภทวีดีโอ
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>รหัส</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                        <input type="text" name="vtype_id" value="<?php echo isset($data->vtype_id)? $data->vtype_id : ''; ?>" class="form-control" id="vtype_id" placeholder="ระบุรหัส" maxlength="20" <?php echo isset($data->vtype_id)? 'readonly' : 'required'; ?> >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ชื่อประเภท</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="vtype_name" value="<?php echo isset($data->vtype_name)? $data->vtype_name : ''; ?>" class="form-control" id="vtype_name" placeholder="ระบุชื่อประเภท" maxlength="200" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>รายละเอียด</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <textarea class="form-control" name="vtype_detail" id="vtype_detail" placeholder="รุบุรายละเอียด" required><?php echo isset($data->vtype_detail)? $data->vtype_detail : ''; ?></textarea> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="hidden" name="for" value="<?php echo isset( $for )? $for : ''; ?>">
                                <button type="submit" class="btn btn-primary">ตกลง</button>
                                <a href="type_video_manage.php" class="btn btn-warning">ยกเลิก</a>
                            </div>
                        </form>
                        </div>
                    </div>
                </div><!-- col-lg-12 -->
                
            </div><!-- row -->
        </div><!-- page-wrapper -->
    </div>
    <?php include('_js.php') ?>
</body>

</html>
