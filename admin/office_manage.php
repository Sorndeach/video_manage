<?php
    session_start();
    include('../dbconn.inc.php');

    if( isset($_POST['office_name']) && !empty($_POST['office_name']) ){
        $office_id = $_POST['office_id'];
        $office_name = $_POST['office_name'];
        $office_address = $_POST['office_address'];
        $office_phone = $_POST['office_phone'];
        $for = $_POST['for'];

        if( !empty($for) && $for == 'edit' ){
            $sql = "UPDATE office SET 
            office_name = '$office_name', 
            office_address = '$office_address',
            office_phone = '$office_phone'
            WHERE office_id = '$office_id'";
            @$mysqli->query($sql) or die($mysqli->error);
        }else{
            $sql = "INSERT INTO office VALUES('$office_id','$office_name','$office_address','$office_phone')";
            @$mysqli->query($sql) or die($mysqli->error);
        }
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="office_list.php";
        </script>
<?php
    }

    if($_GET){
        $office_id = $_GET['office_id'];
        $for = $_GET['for'];

        if( $for == 'edit' ){
            $data = query1("SELECT * FROM office WHERE office_id = '$office_id'");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</title>
    <?php include('_css.php'); ?>
    <style type="text/css">
        tr th{
            text-align: center;
        }
    </style>
</head>

<body>

    <div id="wrapper">
        
    <?php include('_navtop.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ศูนย์รวมวีดิโอการรักษา แพทย์แผนไทย</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> เพิ่มหน่วยงาน
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>รหัส</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                        <input type="text" name="office_id" value="<?php echo isset($data->office_id)? $data->office_id : ''; ?>" class="form-control" id="office_id" placeholder="ระบุรหัส" maxlength="20" <?php echo isset($data->office_id)? 'readonly' : 'required'; ?> >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>ชื่อหน่วยงาน</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="office_name" value="<?php echo isset($data->office_name)? $data->office_name : ''; ?>" class="form-control" id="office_name" placeholder="ระบุชื่อหน่วยงาน" maxlength="200" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>เบอร์โทร</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <input type="text" name="office_phone" value="<?php echo isset($data->office_phone)? $data->office_phone : ''; ?>" class="form-control" id="office_phone" placeholder="รุบุเบอร์โทร" maxlength="10" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ที่อยู่</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <textarea class="form-control" name="office_address" placeholder="ระบุที่อยู่" maxlength="500"><?php echo isset($data->office_address)? $data->office_address : ''; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="hidden" name="for" value="<?php echo isset( $for )? $for : ''; ?>">
                                <button type="submit" class="btn btn-primary">ตกลง</button>
                                <a href="office_manage.php" class="btn btn-warning">ยกเลิก</a>
                            </div>
                        </form>
                        </div>
                    </div>
                </div><!-- col-lg-12 -->
            </div><!-- row -->
        </div><!-- page-wrapper -->
    </div>
    <?php include('_js.php') ?>
</body>

</html>
