<?php
    session_start();
    include('dbconn.inc.php');

    if($_POST){
        $user = $_POST['user_user'];
        $pass = $_POST['user_pass'];

        $sql = "SELECT * FROM user WHERE user_user = '$user' AND user_pass = '$pass' ";
        $query = $mysqli->query($sql);
        $data = $query->fetch_object();
        
        if( !empty($data) ){
            $_SESSION['user_id'] = $data->user_id;
            $_SESSION['user_name'] = $data->user_name;

            if($data->user_status == 'admin'){
                echo '
                    <script type="text/javascript">
                        window.location.href="admin/index.php";
                    </script>
                ';
            }else{
                 echo '
                    <script type="text/javascript">
                        window.location.href="index.php";
                    </script>
                ';
            }

        }else{

        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">กรุณาล็อคอินเพื่อเข้าใช้ระบบ</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" id="fromlogin">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="กรอกชื่อผู้ใช้" name="user_user" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="กรอกรหัสผ่าน" name="user_pass" type="password" value="">
                                </div>
                                <a href="register_user.php" style="margin-bottom: 10px;">สมัครสมาชิก</a>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
