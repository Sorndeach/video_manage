<?php
    session_start();
    include('dbconn.inc.php');
    if( !isset($_SESSION['user_id']) && empty($_SESSION['user_id']) ){
        echo "<script>
        window.location.href='login.php';
        </script>";
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px 0px 50px 0px;">
            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;text-align: center;">
                <h2 style="color:#eea236;">รายการวีดีโอ</h2>
            </div>
            <div class="col-md-6 col-md-offset-3" style="margin-bottom: 20px;">
                <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>"  method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" name="filter_name" placeholder="กรอกข้อมูลเพื่อค้นหา...">
                        <span class="input-group-btn">
                            <button class="btn btn-warning" type="submit">ค้นหา</button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <table width="100%" class="table table-striped table-bordered table-hover" id="user-table">
                    <thead>
                        <tr>
                            <th style="width: 150px;">ลำดับ</th>
                            <th>ชื่อวีดีโอ</th>
                            <th style="width: 250px;">ชื่อประเภท</th>
                            <th style="width: 150px;">ดูวีดีโอ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $item_per_page = 20;
                        $page_url = "index.php";
                        if(isset($_GET["page"])){ 
                            $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
                            if(!is_numeric($page_number)){die('Invalid page number!');} 
                        }else{
                            $page_number = 1; 
                        }

                        $results = $mysqli->query("SELECT COUNT(*) FROM video");
                        $get_total_rows = $results->fetch_row();

                        $total_pages = ceil($get_total_rows[0]/$item_per_page);
                        $page_position = (($page_number-1) * $item_per_page); 

                        // ค้นหาข้อมูล
                        if( isset($_POST['filter_name']) && !empty( $_POST['filter_name'] ) ){
                            $filter_name = $_POST['filter_name'];
                            $sql = "SELECT video.*,video_type.vtype_name
                            FROM video
                            LEFT JOIN video_type ON video_type.vtype_id = video.vtype_id
                            WHERE video.video_name LIKE '%$filter_name%'
                            OR video_type.vtype_name LIKE '%$filter_name%'
                            ORDER BY video_id ASC LIMIT $page_position, $item_per_page";
                        }else{
                            $sql = "SELECT video.*,video_type.vtype_name
                            FROM video
                            LEFT JOIN video_type ON video_type.vtype_id = video.vtype_id
                            ORDER BY video_id ASC LIMIT $page_position, $item_per_page";
                        }
                        $query = $mysqli->query($sql);
                        $i=1;
                        while($data = $query->fetch_object()) : 
                    ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $i; ?></td>
                            <td><?php echo $data->video_name?></td>
                            <td><?php echo $data->vtype_name?></td>
                            <td style="text-align: center;">
                                <a href="show_detail.php?video_id=<?php echo $data->video_id?>" class="btn btn-primary">ดูวีด๊โอ</a>
                            </td>
                        </tr>
                    <?php
                        $i++;
                        endwhile;
                    ?>
                        <tr>
                            <td colspan="5" style="text-align: center;font-weight: bold;">รวม <?php echo $i-1 ;?> รายการ</td>
                        </tr>
                        <tr>
                            <td colspan="5"><?php echo paginate($item_per_page, $page_number, $get_total_rows[0], $total_pages, $page_url); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
