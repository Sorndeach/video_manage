<?php
    session_start();
    include('dbconn.inc.php');

    if( $_POST ){
        $user_id = $_POST['user_id'];
        $user_name = $_POST['user_name'];
        $user_user = $_POST['user_user'];
        $user_pass = $_POST['user_pass'];
        $user_phone = $_POST['user_phone'];
        $user_add = $_POST['user_add'];
        $user_status = 'user';

        $sql = "SELECT * FROM user WHERE user_user = '$user_user'";
        $query = $mysqli->query($sql);
        if($query->num_rows){
            exit("<script>alert('Username ของท่านซ้ำครับ');history.back();</script>");
        }
        $sql = "INSERT INTO user VALUES('$user_id','$user_name','$user_user','$user_pass','$user_phone','$user_add','$user_status')";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="login.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">สมัครสมาชิิก</h3>
                    </div>
                    <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="form-group">
                                <label>รหัส</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                    <input type="text" name="user_id" class="form-control" id="user_id" placeholder="ระบุรหัส" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อ-นามสกุล</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="user_name" class="form-control" id="user_name" placeholder="ระบุชื่อ-นามสกุล" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อผู้ใช้</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                    <input type="text" name="user_user" class="form-control" id="user_user" placeholder="รุบุชื่อผู้ใช้" maxlength="30" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>รหัสผ่าน</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input type="password" name="user_pass" class="form-control" id="user_pass" placeholder="ระบุรหัสผ่าน" maxlength="30" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>โทรศัพท์</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                                    <input type="text" name="user_phone" class="form-control" id="user_phone" placeholder="ระบุโทรศัพท์" maxlength="10" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <textarea class="form-control" name="user_add" placeholder="ระบุที่อยู่" maxlength="500"></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ตกลง</button>
                            <a href="login.php" class="btn btn-warning">ยกเลิก</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
